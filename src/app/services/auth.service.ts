import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public askForLogout: boolean;
  constructor(private _firebaseAuth: AngularFireAuth, private router: Router) {
    this.askForLogout = false;
  }

  signUpWithEmailAndPassword(email: string, password: string) {
    return this._firebaseAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  signInWithEmailAndPassword(email: string, password: string) {
    return this._firebaseAuth.auth.signInWithEmailAndPassword(email, password);
  }

  signInWithTwitter() {
    return this._firebaseAuth.auth.signInWithRedirect(
      new firebase.auth.TwitterAuthProvider()
    )
  }

  signInWithFacebook() {
    return this._firebaseAuth.auth.signInWithRedirect(
      new firebase.auth.FacebookAuthProvider()
    )
  }

  signInWithGoogle() {
    return this._firebaseAuth.auth.signInWithRedirect(
      new firebase.auth.GoogleAuthProvider()
    )
  }

  getUserDetails(): firebase.User {
    return this._firebaseAuth.auth.currentUser;
  }

  isLoggedIn() {
    if (this._firebaseAuth.auth.currentUser == null) {
      return false;
    } else {
      return true;
    }
  }

  logout() {
    this._firebaseAuth.auth.signOut()
      .then((res) => {
        this.askForLogout = true;
        this.router.navigate(['/login']);
      });
  }
}