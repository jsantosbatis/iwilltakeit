import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Event } from 'shared/event';
@Injectable({
  providedIn: 'root'
})
export class EventService {
  constructor(private db: AngularFirestore) {
  }

  addEvent(data: Event): Promise<firebase.firestore.DocumentReference> {
    return new Promise<firebase.firestore.DocumentReference>((resolve, reject) => {
      this.db.collection<Event>('events').add({ ...data }).then(
        (doc: firebase.firestore.DocumentReference) => {
          resolve(doc);
        }),
        (reason) => {
          reject(reason);
        };
    });
  }

  editEvent(data: Event): Promise<Event> {
    return new Promise<Event>((resolve, reject) => {
      this.db.doc<Event>(`events/${data.id}`).update({ ...data }).then(
        () => {
          resolve(data);
        },
        (reason) => {
          reject(reason);
        }
      );
    });
  }

  deleteEvent(id: string) {

  }

  getEvents(userId): Observable<Event[]> {
    return this.db.collection<Event>('events', ref => ref.where('userId', '==', userId))
      .snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })));
  }

  getEvent(id: string): Observable<Event> {
    return this.db.doc<Event>(`events/${id}`)
      .snapshotChanges().pipe(
        map(changes => {
          const data = changes.payload.data();
          const id = changes.payload.id;
          return { id, ...data };
        }));
  }
}
