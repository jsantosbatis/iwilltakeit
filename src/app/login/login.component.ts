import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'services/auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;
  model: { email: string, password: string };
  loginMode: boolean;
  constructor(public auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.model = { email: '', password: '' };
    this.loginMode = true;
  }

  onSubmit() {
    if (this.loginMode) {
      this.loginWithEmailAndPassword();
    } else {
      this.signUpWithEmailAndPassword();
    }
  }

  switchMode(loginMode: boolean) {
    this.loginMode = loginMode;
  }

  loginWithEmailAndPassword() {
    this.auth.signInWithEmailAndPassword(this.model.email, this.model.password).then(() => {
      this.router.navigate(['/home']);
    }, (error) => {
      alert(error);
    });
  }

  signUpWithEmailAndPassword() {
    this.auth.signUpWithEmailAndPassword(this.model.email, this.model.password).then(() => {
      this.router.navigate(['/home']);
    }, (error) => {
      alert(error);
    });
  }

  signInWithGoogle() {
    this.auth.signInWithGoogle();
  }

  signInWithTwitter() {
    this.auth.signInWithTwitter();
  }

  signInWithFacebook() {
    this.auth.signInWithFacebook();
  }

  logout() {
    this.auth.logout();
  }
}
