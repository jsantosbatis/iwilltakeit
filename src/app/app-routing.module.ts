import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'home/home.component';
import { EventComponent } from 'event/event.component';
import { LoginComponent } from 'login/login.component';
import { EventDetailComponent } from 'event-detail/event-detail.component';
import { RedirectGuard } from 'guards/redirect.guard';
import { LoggedGuard } from 'guards/logged.guard.';
import { EventInfoComponent } from 'event/event-info/event-info.component';
import { EventGuestComponent } from 'event/event-guest/event-guest.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [RedirectGuard]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [LoggedGuard]
  },
  {
    path: 'event/create',
    component: EventDetailComponent,
    canActivate: [LoggedGuard]
  },
  {
    path: 'event/:id/edit',
    component: EventDetailComponent,
    canActivate: [LoggedGuard]
  },
  {
    path: 'event/:id',
    component: EventComponent,
    canActivate: [LoggedGuard],
    children: [
      {
        path: 'details',
        component: EventInfoComponent,
        canActivate: [LoggedGuard],
      },
      {
        path: 'guest',
        component: EventGuestComponent,
        canActivate: [LoggedGuard],
      }
    ]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
