import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EventService } from 'services/event.service';
import { NgForm } from '@angular/forms';
import { Event } from 'shared/event';
import { AuthService } from 'services/auth.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {
  @ViewChild('eventForm') form: NgForm;
  model: Event = new Event();
  date: string;
  time: string;
  insertMode: boolean;
  constructor(private eventService: EventService, private router: Router, private route: ActivatedRoute, auth: AuthService) {
    this.route.params.subscribe((params: Params) => {
      if (params['id']) { //Edit mode
        this.insertMode = false;
        eventService.getEvent(params['id']).subscribe((data) => {
          this.model = data;
          this.date = formatDate(this.model.dateTime, 'yyyy-MM-dd', 'fr-BE');
          this.time = formatDate(this.model.dateTime, 'H:MM', 'fr-BE');
        });
      } else {
        this.insertMode = true;
        this.model.userId = auth.getUserDetails().uid;
      }
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    const date = new Date(this.date);
    date.setHours(parseInt(this.time.split(':')[0]), parseInt(this.time.split(':')[1]));
    this.model.dateTime = date.getTime();
    if (this.insertMode) {
      this.eventService.addEvent(this.model).then(
        (data) => this.router.navigate(['event', data.id, 'details'])
      );
    } else {
      this.eventService.editEvent(this.model).then(
        (data) => this.router.navigate(['event', data.id, 'details'])
      );
    }
  }
}
