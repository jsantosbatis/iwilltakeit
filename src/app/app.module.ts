import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule, LOCALE_ID } from '@angular/core';
import { MahModule } from '@castelreng-private/mah';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { EventTileComponent } from './event-tile/event-tile.component';
import { registerLocaleData } from '@angular/common';
import locale from '@angular/common/locales/fr-BE';
registerLocaleData(locale, 'fr-BE');
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { EventComponent } from './event/event.component';
import { LoginComponent } from './login/login.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { RedirectGuard } from 'guards/redirect.guard';
import { AuthService } from 'services/auth.service';
import { LoggedGuard } from 'guards/logged.guard.';
import { EventInfoComponent } from './event/event-info/event-info.component';
import { EventGuestComponent } from './event/event-guest/event-guest.component';

@NgModule({
  declarations: [
    HomeComponent,
    EventTileComponent,
    AppComponent,
    EventComponent,
    LoginComponent,
    EventDetailComponent,
    EventInfoComponent,
    EventGuestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    MahModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule // imports firebase/storage only needed for storage features
  ],
  providers: [
    {
      provide: LOCALE_ID, useValue: "fr-BE"
    },
    RedirectGuard,
    LoggedGuard,
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
