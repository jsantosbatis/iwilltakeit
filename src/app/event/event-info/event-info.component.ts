import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Event } from 'shared/event';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-event-info',
  templateUrl: './event-info.component.html',
  styleUrls: ['./event-info.component.scss']
})
export class EventInfoComponent implements OnInit {
  private eventDoc: AngularFirestoreDocument<Event>;
  eventObservable: Observable<Event>;
  event: Event = null;
  constructor(private db: AngularFirestore, private router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.parent.paramMap.subscribe(params => {
      this.eventDoc = this.db.doc<Event>(`events/${params.get("id")}`);
      this.eventObservable = this.eventDoc.snapshotChanges().pipe(
        map(actions => {
          const data = actions.payload.data() as Event;
          const id = actions.payload.id;
          return { id, ...data };
        }));
      this.eventObservable.subscribe(
        (data: Event) => {
          this.event = data;
        }
      );
    })
  }

  editEvent() {
    this.router.navigate(['event', this.event.id, 'edit']);
  }

  participate(isComing: boolean) {
    this.eventDoc.update({ isComing: isComing }).then(() => {
      this.event.isComing = isComing;
    })
  }

}
