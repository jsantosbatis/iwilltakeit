import { Component, OnInit } from '@angular/core';
import { Event } from 'shared/event';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { EventService } from 'services/event.service';
import { AuthService } from 'services/auth.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  events: Observable<Event[]>;
  bookList: [];
  constructor(private router: Router, private eventService: EventService, private authService: AuthService) { }

  addEvent() {
    this.router.navigate(['event/create']);
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit() {
    this.events = this.eventService.getEvents(this.authService.getUserDetails().uid);
  }

}
