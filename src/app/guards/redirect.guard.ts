import { Injectable, NgZone } from '@angular/core';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from 'services/auth.service';

@Injectable({
  providedIn: 'root'
})
/**
 * RedirectGuard is attached to the login page to redirect to the home page after a signIn/LogIn.
 * It also redirect to the home page if the user try to access to login page while he stills logged.
 */
export class RedirectGuard implements CanActivate {
  path: import("@angular/router").ActivatedRouteSnapshot[];
  route: import("@angular/router").ActivatedRouteSnapshot;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private ngZone: NgZone,
    private authService: AuthService
  ) { }

  canActivate(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.afAuth.auth.getRedirectResult()
        .then((userCredential: firebase.auth.UserCredential) => {
          if (this.authService.askForLogout) {
            this.authService.askForLogout = false; //Reset for next login
            resolve(true);
          } else if (userCredential.user) {
            resolve(false);
            //Workaround see issue https://github.com/angular/angular/issues/25837
            this.ngZone.run(() => this.router.navigate(['/home']).then());
          } else if (this.afAuth.auth.currentUser && userCredential.user == null) {
            //Logged from email/password and trying to go back to login page
            this.ngZone.run(() => this.router.navigate(['/home']).then());
            resolve(false);
          } else {
            resolve(true);
          }
        });
    });
  }
}
