import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
/**
 * LoggedGuard it is attached to each page to ensure that the cuurrent user is logged,
 * otherwise he will be redirected to the login page.
 */
export class LoggedGuard implements CanActivate {
  path: import("@angular/router").ActivatedRouteSnapshot[];
  route: import("@angular/router").ActivatedRouteSnapshot;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router
  ) { }

  canActivate(): boolean {
    if (this.afAuth.auth.currentUser != null) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
