import { firestore } from 'firebase';

export class Event {
    id: string;
    name: string;
    totalParticipants: number;
    participants: number;
    commonPot: number;
    dateTime: number;
    description: string;
    isComing: boolean;
    streetName: string;
    city: string;
    postalCode: string;
    userId: string;
    constructor() {
        this.participants = 0;
        this.totalParticipants = 0;
        this.commonPot = 0;
        this.isComing = false;
    }
};