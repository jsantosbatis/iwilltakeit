// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC06Mh421JRC1bpMeuJXCKyyrReTsMU-sc",
    authDomain: "test-firebase-4521b.firebaseapp.com",
    databaseURL: "https://test-firebase-4521b.firebaseio.com",
    projectId: "test-firebase-4521b",
    storageBucket: "test-firebase-4521b.appspot.com",
    messagingSenderId: "390419807703",
    appId: "1:390419807703:web:d42b675765d365a9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
